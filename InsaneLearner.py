"""  		   	  			    		  		  		    	 		 		   		 		  
A simple wrapper for linear regression.  (c) 2015 Tucker Balch  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
Copyright 2018, Georgia Institute of Technology (Georgia Tech)  		   	  			    		  		  		    	 		 		   		 		  
Atlanta, Georgia 30332  		   	  			    		  		  		    	 		 		   		 		  
All Rights Reserved  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
Template code for CS 4646/7646  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
Georgia Tech asserts copyright ownership of this template and all derivative  		   	  			    		  		  		    	 		 		   		 		  
works, including solutions to the projects assigned in this course. Students  		   	  			    		  		  		    	 		 		   		 		  
and other users of this template code are advised not to share it with others  		   	  			    		  		  		    	 		 		   		 		  
or to make it available on publicly viewable websites including repositories  		   	  			    		  		  		    	 		 		   		 		  
such as github and gitlab.  This copyright statement should not be removed  		   	  			    		  		  		    	 		 		   		 		  
or edited.  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
We do grant permission to share solutions privately with non-students such  		   	  			    		  		  		    	 		 		   		 		  
as potential employers. However, sharing with other current or future  		   	  			    		  		  		    	 		 		   		 		  
students of CS 7646 is prohibited and subject to being investigated as a  		   	  			    		  		  		    	 		 		   		 		  
GT honor code violation.  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
-----do not edit anything above this line---

Student Name: Brandon Sheffield
GT User ID: bsheffield7
GT ID: 903312988
"""

#1
import numpy as np, DTLearner, RTLearner, BagLearner

#2
class InsaneLearner(BagLearner.BagLearner):

    #3
    def __init__(self, bag_learner=BagLearner.BagLearner, learner=DTLearner.DTLearner,
                 bags=20, verbose=False, kwargs={"leaf_size:1"}, boost=False):
        """
        A wrapper for BagLearner using a selected Decision Tree Learner.
        :param bag_learner: The specified BagLearner.  Spoiler alert:  there is only one.
        :param learner: Specify which learner to use:  LinRegLearner, DTLearner, or RTLearner
        :param bags: The number of bags.  Default is 20.
        :param verbose: print out information for debugging
        :param kwargs:
        :param boost:
        """

        #4
        BagLearner.BagLearner.__init__(self, learner=learner, bags=bags, verbose=verbose, kwargs=kwargs, boost=boost)

        #5
        self.bag_learners = [bag_learner(learner=learner, kwargs=kwargs) for _ in range(bags)]

    #6
    def author(self):
        """
        Returns Georgia Tech username.
        :return:
        """

        #7
        return 'bsheffield7'

    #8
    def addEvidence(self, dataX, dataY):
        """
        @summary: Add training data to learner
        @param dataX: X values of data to add
        @param dataY: the Y training values
        """

        #9
        [b.addEvidence(dataX, dataY) for b in self.bag_learners]

    #10
    def query(self, points):
        """
        @summary: Estimate a set of test points given the model we built.
        @param points: should be a numpy array with each row corresponding to a specific query.
        @returns the estimated values according to the saved model.
        """

        #11
        return np.mean(np.array([learner.query(points) for learner in self.bag_learners]), axis=0)
