"""  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
Test a learner.  (c) 2015 Tucker Balch  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
Copyright 2018, Georgia Institute of Technology (Georgia Tech)  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
Atlanta, Georgia 30332  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
All Rights Reserved  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
Template code for CS 4646/7646  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
Georgia Tech asserts copyright ownership of this template and all derivative  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
works, including solutions to the projects assigned in this course. Students  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
and other users of this template code are advised not to share it with others  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
or to make it available on publicly viewable websites including repositories  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
such as github and gitlab.  This copyright statement should not be removed  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
or edited.  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
We do grant permission to share solutions privately with non-students such  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
as potential employers. However, sharing with other current or future  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
students of CS 7646 is prohibited and subject to being investigated as a  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
GT honor code violation.  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
-----do not edit anything above this line---

Student Name: Brandon Sheffield
GT User ID: bsheffield7
GT ID: 903312988
"""

import numpy as np
import math
import time
import matplotlib.pyplot as plt
import sys
import util
import LinRegLearner as lrl
import DTLearner as dtl
import RTLearner as rtl
import BagLearner as bl
import InsaneLearner as il


def evaluate_learner(learner, train_x, train_y, test_x, test_y, in_sample=True, out_sample=True):
    """
    Evaluate the provided learner.
    :param learner: LinRegLearner, DTLearner, RTLearner, BagLearner, InsaneLearner
    :param train_x:
    :param train_y:
    :param test_x:
    :param test_y:
    :return:
    """

    start_time = time.time()
    learner.addEvidence(train_x, train_y)

    corr_in = [[0,0]]
    corr_out = [[0,0]]
    rmse_in = 0
    rmse_out = 0
    predY_in = 0
    predY_out = 0

    if in_sample:
        predY_in = learner.query(train_x)
        rmse_in = math.sqrt(((train_y - predY_in) ** 2).sum() / train_y.shape[0])
        corr_in = np.corrcoef(predY_in, y=train_y)

    if out_sample:
        predY_out = learner.query(test_x)
        rmse_out = math.sqrt(((test_y - predY_out) ** 2).sum() / test_y.shape[0])
        corr_out = np.corrcoef(predY_out, y=test_y)

    end_time = time.time()
    exec_time = float(end_time - start_time)
    result = {"rmse_in": rmse_in, "rmse_out": rmse_out, "corr_out": corr_out[0][1], "corr_in": corr_in[0][1],
              "leaf_size": learner.leaf_size, "exec_time": exec_time, "predY_in": predY_in, "predY_out": predY_out}
              #"test_x": test_x, "train_x": train_x, "test_y": test_y, "train_y": train_y}

    return result


def evaluate_learner_overfitting(learner_class):

    learner_results = list()

    cnt = 500
    for i in range(cnt):
        learner = learner_class(verbose=False, leaf_size=i + 1)
        learner_results.append(evaluate_learner(learner, train_x, train_y, test_x, test_y, in_sample=False))

    leafs = np.zeros(len(learner_results))
    learner_rmses = np.zeros(len(learner_results))
    for i in range(len(learner_results)):
        leafs[i] = learner_results[i]["leaf_size"]
    for i in range(len(learner_results)):
        learner_rmses[i] = learner_results[i]["rmse_out"]

    plt.clf()
    plt.xlabel("Leaf Size")
    plt.ylabel("RMSE")
    #import pandas as pd
    #df = pd.DataFrame(leafs, learner_rmses, columns=["leaf_size","rmse"])
    plt.plot(leafs, learner_rmses, 'b--')

    if learner_class is dtl.DTLearner:
        plt.title("DTLearner Overfitting")
        plt.savefig("DTLearner_Overfitting.png", format="png")
    elif learner_class is bl.BagLearner:
        plt.title("DTLearner Bagging Overfitting")
        plt.savefig("DTLearner_Bagging_Overfitting.png", format="png")

def compare_dtl_vs_rtl(is_graph=True):
    """
    Produces a graph that compares the Decision Tree Learner against Random Tree Leaner
    :param is_graph:
    :return:
    """

    cnt = 500

    dtl_results = list()
    rtl_results = list()

    for i in range(cnt):
        dtl_learner = dtl.DTLearner(verbose=False, leaf_size=i + 1)
        dtl_results.append(evaluate_learner(dtl_learner, train_x, train_y, test_x, test_y, in_sample=False))

        rtl_learner = rtl.RTLearner(verbose=False, leaf_size=i + 1)
        rtl_results.append(evaluate_learner(rtl_learner, train_x, train_y, test_x, test_y, in_sample=False))

    if is_graph:
        plt.clf()
        plt.xlabel("Leaf Size")
        plt.ylabel("Correlation")
        leafs = np.zeros(len(dtl_results))
        dtl_corrs = np.zeros(len(dtl_results))
        rtl_corrs = np.zeros(len(rtl_results))
        for i in range(len(dtl_results)):
            leafs[i] = dtl_results[i]["leaf_size"]
        for i in range(len(dtl_results)):
            dtl_corrs[i] = dtl_results[i]["corr_out"]
        for i in range(len(rtl_results)):
            rtl_corrs[i] = rtl_results[i]["corr_out"]
        plt.plot(leafs, dtl_corrs, 'b--', label="DTL")
        plt.plot(leafs, rtl_corrs, 'r--', label="RTL")
        plt.legend(loc=0)
        plt.title("Correlation to Leaf Size")
        plt.savefig("Correlation_vs_Leaf_Size.png", format="png")

        plt.clf()
        plt.xlabel("Leaf Size")
        plt.ylabel("Execution Time")
        rtl_exec_times = np.zeros(len(rtl_results))
        dtl_exec_times = np.zeros(len(rtl_results))
        for i in range(len(dtl_results)):
            dtl_exec_times[i] = dtl_results[i]["exec_time"]
        for i in range(len(rtl_results)):
            rtl_exec_times[i] = rtl_results[i]["exec_time"]
        plt.plot(leafs, dtl_exec_times, 'b--', label="DTL")
        plt.plot(leafs, rtl_exec_times, 'r--', label="RTL")
        plt.legend(loc=0)
        plt.title("Leaf Size vs Execution Time")
        plt.savefig("Leaf_Size_vs_Execution_Time.png", format="png")

        '''
        plt.clf()
        plt.xlabel("Predict")
        plt.ylabel("Test")
        rtl_predict = np.zeros(len(rtl_results))
        rtl_test = np.zeros(len(rtl_results))
        for i in range(len(rtl_results)):
            rtl_predict[i] = rtl_results[i]["train_y"]
        for i in range(len(rtl_results)):
            rtl_test[i] = rtl_results[i]["test_y"]
        plt.plot(rtl_predict, rtl_test, "b--")
        plt.title("RTL Correlation")
        plt.savefig("RTL_Correlation.png", format="png")

        plt.clf()
        plt.xlabel("Predict")
        plt.ylabel("Test")
        dtl_predict = np.zeros(len(dtl_results))
        dtl_test = np.zeros(len(dtl_results))
        for i in range(len(dtl_results)):
            dtl_predict[i] = dtl_results[i]["train_y"]
        for i in range(len(dtl_results)):
            dtl_test[i] = dtl_results[i]["test_y"]
        plt.plot(dtl_predict, dtl_test, "b--")
        plt.title("DTL Correlation")
        plt.savefig("DTL_Correlation.png", format="png")
        '''

        print("Average RTL Execution Time:  " + str(np.mean(rtl_exec_times)))
        print("Average DTL Execution Time:  " + str(np.mean(dtl_exec_times)))

def report_q1():
    """
    Does overfitting occur with respect to leaf_size? Use the dataset Istanbul.csv with DTLearner. For which values of leaf_size
    does overfitting occur? Use RMSE as your metric for assessing overfitting. Support your assertion with graphs/charts. (Don't use bagging).
    :return:
    """

    evaluate_learner_overfitting(dtl.DTLearner)


def report_q2():
    """
    Can bagging reduce or eliminate overfitting with respect to leaf_size? Again use the dataset Istanbul.csv with DTLearner. To
      investigate this choose a fixed number of bags to use and vary leaf_size to evaluate. Provide charts to validate your
      conclusions. Use RMSE as your metric.
    :return:
    """
    evaluate_learner_overfitting(bl.BagLearner)

def report_q3():
    """
    Quantitatively compare "classic" decision trees (DTLearner) versus random trees (RTLearner). In which ways is one method better
      than the other? Provide at least two quantitative measures. Important, using two similar measures that illustrate the same
      broader metric does not count as two. (For example, do not use two measures for accuracy.) Note for this part of the report
      you must conduct new experiments, don't use the results of the experiments above for this(RMSE is not allowed as a new experiment).
    :return:
    """

    #time, bias, correlation
    #correlation np.corrcoef() +1 strong correlation -1 bad correlation 0 no correlation
    #typically as rms error increases, correlation decreases

    compare_dtl_vs_rtl(is_graph=True)


if __name__ == "__main__":
    """
    Evaluate three learning algorithms: A "classic" Decision Tree learner, a Random Tree learner, and a Bootstrap Aggregating learner.
    """

    if len(sys.argv) != 2:
        print("Usage: python testlearner.py <filename>")
        sys.exit(1)
    data = np.ndarray([])

    with util.get_learner_data_file(sys.argv[1]) as f:
        data = np.genfromtxt(f, delimiter=',')
        # Skip the first column Istanbul dataset
        if 'Istanbul.csv' in sys.argv[1]:
            data = data[1:, 1:]

    # compute how much of the data is training and testing  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
    train_rows = int(0.6 * data.shape[0])
    test_rows = data.shape[0] - train_rows

    # separate out training and testing data  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
    train_x = data[:train_rows, 0:-1]
    train_y = data[:train_rows, -1]
    test_x = data[train_rows:, 0:-1]
    test_y = data[train_rows:, -1]

    print(f"{test_x.shape}")
    print(f"{test_y.shape}")

    report_q1()
    report_q2()
    report_q3()
